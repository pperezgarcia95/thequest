import os
import pygame
import settings as s
import db.database as db




# https://stackoverflow.com/questions/4183208/how-do-i-rotate-an-image-around-its-center-using-pygame
def rot_center(image, angle, x, y):
    """ Rotate an image and return image rotated and new rect"""
    
    rotated_image = pygame.transform.rotate(image, angle)
    new_rect = rotated_image.get_rect(center = image.get_rect(center = (x, y)).center)

    return rotated_image, new_rect


def load_png(name, dimensions, scale = True):
    """ Load image and return image object"""
    
    fullname = os.path.join(s.IMAGES_PATH, name)
    try:
        image = pygame.image.load(fullname)
        if image.get_alpha is None:
            image = image.convert()
        else:
            image = image.convert_alpha()
        
        # Change image size
        if scale:
            image = pygame.transform.smoothscale(image, dimensions)
        
    except FileNotFoundError as message:
        print ('Cannot load image: %s' % fullname)
        raise SystemExit(message)
    except pygame.error as message:
        print ('Cannot load image: %s' % fullname)
        raise SystemExit(message)

    return image, image.get_rect()


def get_sprites_sheet(sheet, num_col, num_row, offset, size):
    """ Clip out sprites from a sheet sprites"""
    
    images = []
    
    pos_x = 0
    pos_y = 0
    for i in range(num_row):
        
        # Update row
        pos_y = (size[1] + 2 * offset[1]) * i + offset[1]
        
        for j in range(num_col):
        
            # Update column
            pos_x = (size[0] + 2 * offset[0]) * j + offset[0]
            
            # http://es.uwenku.com/question/p-vdksiamn-ba.html
            images.append(sheet.subsurface(pygame.Rect((pos_x, pos_y), size)))

    return images
    

# https://stackoverflow.com/questions/32590131/pygame-blitting-text-with-an-escape-character-or-newline    
def split_text(font, text):
    """ It separate the text in different lines"""
    
    label = []
    for line in text: 
        label.append(font.render(line, True, s.BLACK))
    
    return label
    
    
def timer(ref_time, threshold_time):
    """ Update timer and indicate when reaches the threshold"""
    
    # Get current raw time
    current_time = pygame.time.get_ticks()
    
    # Update time stamp
    update_time_stamp = (current_time - ref_time[0]) > threshold_time
    if update_time_stamp:
        ref_time[0] += threshold_time
    
    return update_time_stamp


def update_database(conn, values, num_elements):
    """ Updates current database state in both case, insert or update"""
    
    if (num_elements < s.MAX_ROWS_SCOREBOARD):
        values.insert(0, num_elements + 1)
        db.sql_insert_into(conn, values)
        
    elif (num_elements == s.MAX_ROWS_SCOREBOARD):
        db.sql_update(conn, values)
            
            