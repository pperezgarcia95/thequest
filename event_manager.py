#https://stackoverflow.com/questions/1904351/python-observer-pattern-examples-tips
class Observer():
    """ An observer for a specific events
    
    Returns: observer object  
    Functions: observe  
    Attributes:
    
    """
    
    _observers = []
    
    def __init__(self):
        self._observers.append(self)
        self._observables = {}
    
    def observe(self, event_name, callback):
        """ Subscribes an observer to an event"""
        
        self._observables[event_name] = callback


class Event():
    """ An event that can trigger to observers
    
    Returns: event object  
    Functions: observe  
    Attributes: name, data, autofire
    
    """
    
    def __init__(self, name, data, autofire = True):
        self.name = name
        self.data = data
        if autofire:
            self.fire()
    
    def fire(self):
        """ Fires an event that triggering the observables"""
        
        for observer in Observer._observers:
            if self.name in observer._observables:
                if self.data is None:
                    observer._observables[self.name]()
                else:
                    observer._observables[self.name](self.data)

