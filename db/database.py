# https://likegeeks.com/es/tutorial-de-python-sqlite3/

import settings as s
import sqlite3
from sqlite3 import Error


def sql_connection():
    """ Creates the database if not exists and makes the connection to it"""
    
    try:
        
        conn = sqlite3.connect('the_quest_database.db')
        
        return conn

    except Error:

        if s.DEBUG or s.DATABASE_DEBUG:
            print(Error)


def sql_table(conn):
    """ Creates the only table in the database if not exists"""
    
    cursorObj = conn.cursor()
    
    query = "CREATE TABLE IF NOT EXISTS scoreboard(id integer PRIMARY KEY, name text, score integer)"
    
    if s.DEBUG or s.DATABASE_DEBUG:
        print(query)

    cursorObj.execute(query)

    conn.commit()

    
def sql_select_order_by_score(conn):
    """ Selects all rows in the scoreboard table ordered by descending score"""

    cursorObj = conn.cursor()
    
    query = "SELECT * FROM scoreboard ORDER BY score DESC"
    
    if s.DEBUG or s.DATABASE_DEBUG:
        print(query)

    cursorObj.execute(query)
    
    return cursorObj.fetchall()


def sql_insert_into(conn, values):
    """ Inserts a new score in the scoreboard table"""
    
    cursorObj = conn.cursor()
    
    query = "INSERT INTO scoreboard VALUES (" + str(values[0]) + ", '" + values[1] + "', " + str(values[2]) + ")"
    
    if s.DEBUG or s.DATABASE_DEBUG:
        print(query)
    
    cursorObj.execute(query)
    
    conn.commit()


def sql_update(conn, values):
    """ Updates a new score in the scoreboard table. Always in the less score."""

    cursorObj = conn.cursor()
    
    query = ("UPDATE scoreboard SET name = '" + values[0] + "', score = " + str(values[1]) + 
        " WHERE id = (SELECT id FROM scoreboard ORDER BY score LIMIT 1)")
    
    if s.DEBUG or s.DATABASE_DEBUG:
        print(query)

    cursorObj.execute(query)
    
    conn.commit()

