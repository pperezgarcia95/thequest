import pygame
import sys
import os
import random
import event_manager as em
import utilities as u
import settings as s
import exceptions as e

import db.database as db 




class UI(em.Observer):     
    """ User Interface
    
    Returns: UI object  
    Functions: resize, show_instructions, update_name_current_score, show_scoreboard,  
               show_continue_message, hide_continue_message, finish_game, draw, update  
    Attributes: level
    
    """
    
    # Avoid to create more than one HUD
    __instance = None
    
    def __init__(self):
    
        if (UI.__instance == None):
            UI.__instance = self
            
        else:
            try:
                raise e.CreateInstanceError(str(type(self).__name__))
                
            except e.CreateInstanceError as error:
                
                print(error.message)
                
                # Exit
                pygame.quit()
                sys.exit(0)
        
        
        # Init state
        self.state = "Init"
        
        # Initialise scoreboard
        self.score = 0
        self.text_score = ""
        self.update_scoreboard = False
        self.found_scoreboard_pos = False
        self.update_database = False
        
        self.rows = []
        self.update_row_pos = 0
        
        # Editable text
        self.text_name_current_score = ""
        
        # Initialise
        self.resize()
        
        # Timer
        self.ref_show_instructions_time = [pygame.time.get_ticks()]
        self.threshold_show_instructions_time = s.SHOW_INSTRUCTIONS_TIME
        
        self.ref_show_continue_message_time = [pygame.time.get_ticks()]
        self.threshold_show_continue_message_time = s.SHOW_COTINUE_MESSAGE_TIME
        
        self.ref_show_scoreboard_time = [pygame.time.get_ticks()]
        self.threshold_show_scoreboard_time = s.SHOW_SCOREBOARD_TIME
        
        
        # Animation
        self.animation_count = 0
        self.duration_factor = 3.5
        
        # Create observer
        em.Observer.__init__(self)
        
        # Subcribe to different events
        self.observe('finish landing', self.show_continue_message)
        self.observe('continue game', self.hide_continue_message)
        self.observe('game over', self.show_scoreboard)
        
        if s.DEBUG or s.UI_DEBUG:
            print("UI is listening.")
        
        
        # Create events
        self.start_game_event = em.Event('start game', None, False)
        self.reinit_game_event = em.Event('reinit game', None, False)
        
        
    def resize(self):
        """ Adaptates scale and positions inside the UI"""
        
        # Get background
        self.background = pygame.Surface((s.SCREEN_WIDTH, s.SCREEN_HEIGHT))
        if (self.state == "Init"):
            self.background = u.load_png('init_background.png', (s.SCREEN_WIDTH, s.SCREEN_HEIGHT))[0]
        
        elif (self.state == "Show instructions"):
            self.background = u.load_png('instructions.png', (s.SCREEN_WIDTH, s.SCREEN_HEIGHT))[0]
        
        elif (self.state == "Show scoreboard"):
            self.background = u.load_png('scoreboard.png', (s.SCREEN_WIDTH, s.SCREEN_HEIGHT))[0]
        
        # Initialise init screen
        # Title
        title_font_size = (s.SCREEN_WIDTH // 10)
        title_font = pygame.font.SysFont(None, title_font_size)
        self.text_title = title_font.render("THE QUEST", True, s.RED)
        text_title_x_position = ((s.SCREEN_WIDTH // 2) - (self.text_title.get_width() // 2))
        self.title_position = (text_title_x_position, s.SCREEN_HUD_HEIGHT)
        
        # Story
        self.story_font_size = (s.SCREEN_HEIGHT // 20)
        self.story_font = pygame.font.Font(None, self.story_font_size)
        
        story_text = ["The Earth planet is devastated,",
            "   could not bear all of our humanity.", "We have been forced to go",
                "   to other planets to survive.", "It's our last chance..."]
        
        self.text_story = u.split_text(self.story_font, story_text)
        padding_x = (s.SCREEN_WIDTH // 8)
        padding_y = title_font_size
        text_story_x_position = padding_x
        text_story_y_position = (s.SCREEN_HUD_HEIGHT + padding_y)
        
        self.story_position = (text_story_x_position, text_story_y_position)
        
        
        # Init game
        init_game_font_size = (s.SCREEN_WIDTH // 25)
        self.init_game_font = pygame.font.Font(None, init_game_font_size)
        
        self.text_init_game = self.init_game_font.render("PRESS " + s.START_KEY_TEXT + " TO PLAY", True, s.YELLOW)
        padding_y = (s.SCREEN_HEIGHT // 10)
        text_init_game_x_position = ((s.SCREEN_WIDTH // 2) - (self.text_init_game.get_width() // 2))
        text_init_game_y_position = (s.SCREEN_HEIGHT - padding_y)
        
        self.init_game_position = (text_init_game_x_position, text_init_game_y_position)
        
        
        # Initialise instructions
        # Instruccions
        instructions_font_size = (s.SCREEN_WIDTH // 20)
        instructions_font = pygame.font.Font(None, instructions_font_size)
        
        
        self.text_instructions = []
        self.text_instructions.append(instructions_font.render("Move the ship", True, s.WHITE))
        self.text_instructions.append(instructions_font.render(" up ", True, s.GREEN))
        self.text_instructions.append(instructions_font.render("and", True, s.WHITE))
        self.text_instructions.append(instructions_font.render(" down ", True, s.GREEN))
        self.text_instructions.append(instructions_font.render("to avoid the", True, s.WHITE))
        self.text_instructions.append(instructions_font.render(" asteroids", True, s.RED))
        
        total_text_instructions_width = 0
        for text in self.text_instructions:
            total_text_instructions_width += text.get_width()
            
        padding_y = (s.SCREEN_HEIGHT // 5)
        text_instructions_x_position = ((s.SCREEN_WIDTH // 2) - (total_text_instructions_width // 2))
        text_instructions_y_position = padding_y
        
        self.instructions_init_position = (text_instructions_x_position, text_instructions_y_position)
        
        
        instructions2_font_size = (s.SCREEN_WIDTH // 25)
        instructions2_font = pygame.font.Font(None, instructions2_font_size)
        
        self.text_instructions2 = instructions2_font.render("W, Up_arrow: up   S, Down_arrow: down   Shift: accelerate", True, s.WHITE)
        
        padding_y = (s.SCREEN_HEIGHT // 10)
        text_instructions2_x_position = ((s.SCREEN_WIDTH // 2) - (self.text_instructions2.get_width() // 2))
        text_instructions2_y_position = (s.SCREEN_HEIGHT - padding_y)
        
        self.instructions2_position = (text_instructions2_x_position, text_instructions2_y_position)
        
        
        # Initialise continue game
        # Continue message
        continue_game_font_size = (s.SCREEN_WIDTH // 15)
        continue_game_font = pygame.font.Font(None, instructions_font_size)
        self.text_continue_message = continue_game_font.render("Level complete! Press " + s.CONTINUE_KEY_TEXT + " to continue", True, s.YELLOW)
        
        half_screen_height_size = (s.SCREEN_HEIGHT // 2)
        half_screen_width_size = (s.SCREEN_WIDTH // 2)
        half_text_continue_message_width_size = (self.text_continue_message.get_width() // 2)
        hallf_text_continue_message_height_size = (self.text_continue_message.get_height() // 2)
        self.x_pos_continue_message = half_screen_width_size - half_text_continue_message_width_size
        self.y_pos_continue_message = half_screen_height_size - hallf_text_continue_message_height_size
        
        
        # Initialise scoreboard
        
        # Title
        scoreboard_font_size = (s.SCREEN_WIDTH // 15)
        self.scoreboard_font = pygame.font.Font(None, scoreboard_font_size)
        self.text_scoreboard_title = self.scoreboard_font.render("SCOREBOARD", True, s.RED)
        
        padding_y = (s.SCREEN_HEIGHT // 10)
        self.scoreboard_position = (half_screen_width_size - (self.text_scoreboard_title.get_width() // 2), padding_y)
        
        # Table headers
        scoreboard_table_headers_font_size = (s.SCREEN_WIDTH // 20)
        self.scoreboard_table_headers_font = pygame.font.Font(None, scoreboard_table_headers_font_size)
        self.text_scoreboard_name_header = self.scoreboard_table_headers_font.render("NAME", True, s.RED)
        
        padding_x_name_header = (s.SCREEN_WIDTH // 8)
        padding_y_name_header = 2 * padding_y + self.text_scoreboard_title.get_height()
        self.scoreboard_name_header_position = (padding_x_name_header, padding_y_name_header)
        
        
        self.text_scoreboard_score_header = self.scoreboard_table_headers_font.render("SCORE", True, s.RED)
        
        padding_x_score_header = s.SCREEN_WIDTH - padding_x_name_header - self.text_scoreboard_name_header.get_width()
        self.scoreboard_score_header_position = (padding_x_score_header, padding_y_name_header)
        
        
        # Table rows
        scoreboard_table_rows_font_size = (s.SCREEN_WIDTH // 25)
        self.scoreboard_table_rows_font = pygame.font.Font(None, scoreboard_table_rows_font_size)
        
        self.img_name_current_score = self.scoreboard_table_rows_font.render(self.text_name_current_score, True, s.BLUE)
        
        
        self.text_score = ""
        self.img_score = self.scoreboard_table_rows_font.render(self.text_score, True, s.BLUE)
        
        self.row_padding_y = self.text_scoreboard_name_header.get_height()
        padding_y_first_row_name = padding_y_name_header + self.row_padding_y + (s.SCREEN_HEIGHT // 75)
        self.text_scoreboard_first_row_name_position = [padding_x_name_header + (self.text_scoreboard_name_header.get_width() // 4), padding_y_first_row_name]
        self.text_scoreboard_first_row_name_init_y_position = self.text_scoreboard_first_row_name_position[1]
        
        
        self.text_scoreboard_first_row_score_position = [padding_x_score_header + (self.text_scoreboard_score_header.get_width() // 4), padding_y_first_row_name]
        self.text_scoreboard_first_row_score_init_y_position = self.text_scoreboard_first_row_score_position[1]
        
        # Cursor
        self.row_size = ((s.SCREEN_WIDTH // 100), self.img_name_current_score.get_height())
        self.text_name_current_score_cursor = pygame.Rect(self.text_scoreboard_first_row_name_position, self.row_size)
        
        self.current_row_cursor = self.text_scoreboard_first_row_name_position
        
        self.update_name_current_score()
        
        
        # Finish message
        scoreboard_finish_message_font_size = (s.SCREEN_WIDTH // 20)
        self.scoreboard_finish_message_font = pygame.font.Font(None, scoreboard_finish_message_font_size)
        self.text_scoreboard_finish_message = self.scoreboard_finish_message_font.render("INSERT YOUR NAME AND PRESS " + s.START_KEY_TEXT, True, s.YELLOW)
        
        text_scoreboard_finish_message_y_position = s.SCREEN_HEIGHT - padding_y
        self.finish_message_position = (half_screen_width_size - (self.text_scoreboard_finish_message.get_width() // 2), text_scoreboard_finish_message_y_position)
    
    
    def show_instructions(self):
        """ Shows intructions on the screen"""
        
        self.state = "Show instructions" 

        # Update background
        self.background = u.load_png('instructions.png', (s.SCREEN_WIDTH, s.SCREEN_HEIGHT))[0]
        
        # Reset timer
        self.ref_show_instructions_time[0] = pygame.time.get_ticks()
    
    
    def update_name_current_score(self):
        """ Updates current player name"""
        
        self.img_name_current_score = self.scoreboard_table_rows_font.render(self.text_name_current_score, True, s.BLUE)
        
        update_x_position = self.current_row_cursor[0] + self.img_name_current_score.get_width()
        self.text_name_current_score_cursor.topleft = (update_x_position, self.current_row_cursor[1])
    
    
    def show_scoreboard(self, values):
        """ Shows scoreboard on the screen. If it is necessary to update it are indicated with update_scoreboard."""
        
        self.update_scoreboard = (self.state == "Hide")
            
        self.state = "Show scoreboard"
        
        # Update background
        self.background = u.load_png('scoreboard.png', (s.SCREEN_WIDTH, s.SCREEN_HEIGHT))[0]
        
        # Update score
        if s.DEBUG or s.UI_DEBUG:
            print("Score: " + str(values[0]))
        
        self.score = values[0]
        
        # Reset
        self.found_scoreboard_pos = False
        self.ref_show_scoreboard_time = [pygame.time.get_ticks()]
        self.update_database = False
        self.text_name_current_score = ""
        
        # Search in database
        self.rows = db.sql_select_order_by_score(values[1])
    
    
    def show_continue_message(self):
        """ Shows continue message on the screen"""
        
        self.state = "Continue message"
        
        # Reset timer
        self.ref_show_continue_message_time[0] = pygame.time.get_ticks()
    
    
    def hide_continue_message(self):
        """ Hides continue message on the screen"""
        
        self.state = "Hide"
    
    
    def finish_game(self):
        """ Returns the game to initial state"""
        
        self.reinit_game_event.fire()

        self.state = "Init"
        
        # Update background
        self.background = u.load_png('init_background.png', (s.SCREEN_WIDTH, s.SCREEN_HEIGHT))[0]

        if s.DEBUG or s.UI_DEBUG:
            print("Reinit game event fire")
    
    
    def draw(self, screen, db_connection):
        """ Draws the UI on the screen"""
        
        if ((self.state != "Hide") and (self.state != "Continue message")):
        
            # Background
            screen.blit(self.background, (0, 0))
        
        if (self.state == "Init"):

            # Title
            screen.blit(self.text_title, self.title_position)
            
            # Story
            padding_y = 0
            for line in range(len(self.text_story)):
                screen.blit(self.text_story[line], (self.story_position[0], self.story_position[1] + (line * self.story_font_size) + (padding_y * line)))
            
            # Init game
            delay = 25
            if ((self.animation_count > delay) and 
                (self.animation_count < (s.FPS * self.duration_factor) - delay)):
                screen.blit(self.text_init_game, self.init_game_position)
                
            elif (self.animation_count > s.FPS * self.duration_factor):
                
                # Reset
                self.animation_count = 0
                
        elif (self.state == "Show instructions"):
            
            text_pos_x = 0
            for text in self.text_instructions:
                screen.blit(text, (self.instructions_init_position[0] + text_pos_x, self.instructions_init_position[1]))
                text_pos_x += text.get_width()
            
            screen.blit(self.text_instructions2, self.instructions2_position)
        
        elif (self.state == "Continue message"):
            screen.blit(self.text_continue_message, (self.x_pos_continue_message, self.y_pos_continue_message))
            
        
        elif (self.state == "Show scoreboard"):
            # Title
            screen.blit(self.text_scoreboard_title, self.scoreboard_position)
            
            # Table headers
            screen.blit(self.text_scoreboard_name_header, self.scoreboard_name_header_position)
            screen.blit(self.text_scoreboard_score_header, self.scoreboard_score_header_position)
            
            # Row contents
            
            self.text_scoreboard_first_row_name_position[1] = self.text_scoreboard_first_row_name_init_y_position
            self.text_scoreboard_first_row_score_position[1] = self.text_scoreboard_first_row_score_init_y_position
            if (len(self.rows) > 0):
                for i in range(len(self.rows)):
                    
                    if ((self.found_scoreboard_pos) and (self.update_row_pos == i)):
                        
                        self.current_row_cursor = self.text_scoreboard_first_row_name_position
                        self.update_name_current_score()
                        
                        # Current Name
                        screen.blit(self.img_name_current_score, self.text_scoreboard_first_row_name_position)

                        # Current Score
                        self.img_score = self.scoreboard_table_rows_font.render(str(self.score), True, s.BLUE)
                        screen.blit(self.img_score, self.text_scoreboard_first_row_score_position)
                        
                        if (i < (s.MAX_ROWS_SCOREBOARD - 1)):
                        
                            # Update text position
                            padding_y = (s.SCREEN_HEIGHT // 75)
                            self.text_scoreboard_first_row_name_position[1] += self.text_scoreboard_name_header.get_height() + padding_y
                            self.text_scoreboard_first_row_score_position[1] += self.text_scoreboard_score_header.get_height() + padding_y
                            
                            # Name
                            self.img_name_current_score = self.scoreboard_table_rows_font.render(str(self.rows[i][1]), True, s.BLUE)
                            screen.blit(self.img_name_current_score, self.text_scoreboard_first_row_name_position)
                            
                            # Score
                            self.img_score = self.scoreboard_table_rows_font.render(str(self.rows[i][2]), True, s.BLUE)
                            screen.blit(self.img_score, self.text_scoreboard_first_row_score_position)
                        
                    else:
                        # Name
                        self.img_name_current_score = self.scoreboard_table_rows_font.render(str(self.rows[i][1]), True, s.BLUE)
                        screen.blit(self.img_name_current_score, self.text_scoreboard_first_row_name_position)
                        
                        # Score
                        self.img_score = self.scoreboard_table_rows_font.render(str(self.rows[i][2]), True, s.BLUE)
                        screen.blit(self.img_score, self.text_scoreboard_first_row_score_position)
                    
                    # Update text position
                    padding_y = (s.SCREEN_HEIGHT // 75)
                    self.text_scoreboard_first_row_name_position[1] += self.text_scoreboard_name_header.get_height() + padding_y
                    self.text_scoreboard_first_row_score_position[1] += self.text_scoreboard_score_header.get_height() + padding_y
            
            
            if (self.update_row_pos == len(self.rows)):
                
                self.current_row_cursor = self.text_scoreboard_first_row_name_position
                self.update_name_current_score()
                
                # Name
                screen.blit(self.img_name_current_score, self.text_scoreboard_first_row_name_position)
                
                # Score
                self.img_score = self.scoreboard_table_rows_font.render(str(self.score), True, s.BLUE)
                screen.blit(self.img_score, self.text_scoreboard_first_row_score_position)
            
            
            if (self.found_scoreboard_pos):
            
                # Cursor
                delay = (s.FPS // 2)
                if ((self.animation_count > delay) and (self.animation_count < s.FPS)):
                    pygame.draw.rect(screen, s.RED, self.text_name_current_score_cursor)
                
                elif (self.animation_count > s.FPS):
                    # Reset
                    self.animation_count = 0
            
                # Finish message
                screen.blit(self.text_scoreboard_finish_message, self.finish_message_position)
    
         
    def update(self):
        """ Updates the UI state"""
        
        if (self.state == "Init"):
            self.animation_count += 1
            
        elif (self.state == "Show instructions"):
            if (u.timer(self.ref_show_instructions_time, self.threshold_show_instructions_time)):
                self.start_game_event.fire()
                
                self.state = "Hide"
                
        elif (self.state == "Continue message"):
            if (u.timer(self.ref_show_continue_message_time, self.threshold_show_continue_message_time)):
                self.finish_game()
        
        
        elif (self.state == "Show scoreboard"):
        
            # Found new score position
            if self.update_scoreboard and not self.found_scoreboard_pos:
                if (len(self.rows) > 0):
                    i = 0
                    while (i < len(self.rows)) and not (self.found_scoreboard_pos):
                        self.found_scoreboard_pos = (self.score > self.rows[i][2]) and not self.found_scoreboard_pos
                        
                        i += 1
                    
                            
                    if (len(self.rows) < s.MAX_ROWS_SCOREBOARD) and not self.found_scoreboard_pos:
                        
                        self.found_scoreboard_pos = True
                        self.update_row_pos = len(self.rows)
                        
                    elif self.found_scoreboard_pos:
                        self.update_row_pos = i - 1
                
                elif (len(self.rows) == 0):

                    self.found_scoreboard_pos = True
                    self.update_row_pos = 0
                    
                self.update_scoreboard = self.found_scoreboard_pos
                
                if s.DEBUG or s.UI_DEBUG:
                    print("Found scoreboard position: " + str(self.found_scoreboard_pos))
                    
                    if (self.found_scoreboard_pos):
                        print("New score position: " + str(self.update_row_pos))
            
            
            if (not self.found_scoreboard_pos) or (self.update_database):
                if (u.timer(self.ref_show_scoreboard_time, self.threshold_show_scoreboard_time)):
                    self.finish_game()
            
            
            self.animation_count += 1

   