import pygame
import os
import random
import event_manager as em
import utilities as u
import settings as s




class Asteroid(pygame.sprite.Sprite):
    """ An asteroid that will move across the screen
    
    Returns: asteroid object  
    Functions: resize, generate_position, draw, update  
    Attributes:
    
    """
    
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.size = random.randint(s.ASTEROID_MIN_SIZE, s.ASTEROID_MAX_SIZE)
        self.scale_size = (s.SCREEN_HEIGHT // self.size)
        self.dimensions = (self.scale_size, self.scale_size)
        
        self.type = random.randint(0, s.ASTEROID_MAX_TYPES - 1)
        self.name = 'asteroid' + str(self.type + 1) + '.png'
        self.image, self.rect = u.load_png(self.name, self.dimensions)
        
        self.scale_factor = (s.SCREEN_WIDTH / s.SCREEN_PREV_WIDTH)
        
        # Generate position and update speed
        self.generate_position()
        
        
        # Create events
        self.increase_score_event = em.Event('increase score', s.SCORE_INCREMENT, False)
    
    
    def resize(self):
        """ Adaptates the asteroid scale and position"""
        
        self.scale_size = (s.SCREEN_HEIGHT // self.size)
        self.dimensions = (self.scale_size, self.scale_size)
        
        aux_rect = self.rect
        self.image, self.rect = u.load_png(self.name, self.dimensions)
        
        # Update position respecting scale
        self.rect.x = 0 if (aux_rect.x == 0) else ((aux_rect.x / s.SCREEN_PREV_WIDTH) * s.SCREEN_WIDTH)
        self.rect.y = 0 if (aux_rect.y == 0) else ((aux_rect.y / s.SCREEN_PREV_HEIGHT) * s.SCREEN_HEIGHT)
        
        # Clamp
        if (self.rect.y + self.dimensions[1] > s.SCREEN_HEIGHT):
            self.rect.y = s.SCREEN_HEIGHT - self.dimensions[1]
            
        if (self.rect.y < s.SCREEN_HUD_HEIGHT):
            self.rect.y = s.SCREEN_HUD_HEIGHT
            
        if (self.rect.y + self.dimensions[1] > s.SCREEN_WIDTH):
            self.rect.y = s.SCREEN_WIDTH - self.dimensions[0]

        # Update speed respecting scale
        self.scale_factor = (s.SCREEN_WIDTH / s.SCREEN_PREV_WIDTH)
        self.speed *= self.scale_factor
    
    
    def generate_position(self):
        """ Generates random asteroid position"""
        
        self.show = False
        
        # Update speed
        # https://stackoverflow.com/questions/6088077/how-to-get-a-random-number-between-a-float-range
        self.speed = random.uniform(s.ASTEROID_INITIAL_TYPE_SPEED[self.type][0], s.ASTEROID_INITIAL_TYPE_SPEED[self.type][1])
        self.speed *= self.scale_factor
        
        self.rect.x = s.SCREEN_WIDTH - self.dimensions[0]
        self.rect.y = random.randint(s.SCREEN_HUD_HEIGHT, s.SCREEN_HEIGHT - self.dimensions[1])
        
        
    # https://stackoverflow.com/questions/22361249/how-can-i-just-draw-a-specific-one-sprite-in-a-group-with-pygame-draw
    def draw(self, screen):
        """ Draws asteroid at the screen"""
        
        if self.show:
            screen.blit(self.image, self.rect)
            
            if s.DEBUG or s.LEVEL_DEBUG:
                pygame.draw.rect(screen, s.RED, self.rect, 1)
       
       
    def update(self):
        """ Updates asteroid position"""
        
        if self.show:
            # Decimal value
            aux_rect = self.rect.x - self.speed
            
            self.rect.x = aux_rect
        
            # Outside screen
            if (aux_rect < 0):
                self.generate_position()
                self.increase_score_event.fire()

