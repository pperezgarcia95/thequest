import pygame
import os
import random
import event_manager as em
import utilities as u
import settings as s

      
class Ship(pygame.sprite.Sprite, em.Observer):
    """ Player ship
    
    Returns: ship object  
    Functions: reinit, resize, draw, update,  
               moveup, movedown, change_speed, stop,  
               loose_one_life, restore, start_landing,  
               continue_game, detect_collision,  
               detect_rect_collision, continue_landing,  
               automove, scan, find_best_gap  
    Attributes: dimensions
    
    """
    
    def __init__(self, dimensions):
        pygame.sprite.Sprite.__init__(self)
        self.rect = None
        self.scan_area = None
        
        self.explosion = False
        self.block = False
        self.waiting_for_landing = False
        self.landing_to_planet = False
        self.stopped = False
        
        
        self.resize(dimensions)
        
        self.rect.midleft = self.area.midleft
        self.movepos = [0, 0]
        self.state = ["Not pressed", "Not pressed"]
        self.is_accelerated = False
        
        self.explosion_sound = pygame.mixer.Sound(os.path.join(s.SOUNDS_PATH, 'explosion_sound.flac'))
        
        
        # IA
        self.dest_pos_y = 0
        self.gaps = []
        self.finding_best_gap = True
        self.found_best_gap = False
        self.complete_scan = False
        
        
        # Create observer
        em.Observer.__init__(self)
        
        # Subcribe to different events
        self.observe('loose one life', self.loose_one_life)
        self.observe('restore', self.restore)
        self.observe('start landing', self.start_landing)
        self.observe('landing to planet', self.continue_landing)
        self.observe('continue game', self.continue_game)
        self.observe('reinit game', self.continue_game)
        
        
        if s.DEBUG or s.SHIP_DEBUG:
            print("Ship is listening.")
        
        # Animation
        self.animation_count = 0
        self.duration_factor = 1.5
        self.finish_animation = False 
    
    
    def reinit(self):
        """ Resets ship to init state"""
        
        self.state = ["Not pressed", "Not pressed"]
        self.rect.midleft = self.area.midleft
        
        # Reset scan position
        self.scan_area.y = s.SCREEN_HUD_HEIGHT
                    
        self.finding_best_gap = True
        self.complete_scan = False
        self.found_best_gap = False

        self.stopped = False
        
    
    def resize(self, dimensions):
        """ Adaptates the ship scale, rotation and position"""  
    
        # Load sprite
        aux_rect = None
        if (self.rect != None):
            aux_rect = self.rect
        
        self.image, self.rect = u.load_png('ship.png', dimensions)
        
        # Restore rotation
        if (self.waiting_for_landing or self.landing_to_planet):
            # Rotate ship to land
            degrees_rot = 180
            self.image, self.rect = u.rot_center(self.image, degrees_rot, self.rect.centerx, self.rect.centery)
        
        # Restore position
        if (aux_rect != None):
            # Update position respecting scale
            self.rect.x = 0 if (aux_rect.x == 0) else ((aux_rect.x / s.SCREEN_PREV_WIDTH) * s.SCREEN_WIDTH)
            self.rect.y = 0 if (aux_rect.y == 0) else ((aux_rect.y / s.SCREEN_PREV_HEIGHT) * s.SCREEN_HEIGHT)
            
            # Clamp
            if (self.rect.y + s.SHIP_SIZE[1] > s.SCREEN_HEIGHT):
                self.rect.y = s.SCREEN_HEIGHT - s.SHIP_SIZE[1]
                
            if (self.rect.y < s.SCREEN_HUD_HEIGHT):
                self.rect.y = s.SCREEN_HUD_HEIGHT
            
            if s.DEBUG or s.SHIP_DEBUG:
                print("Previous screeen height: " + str(s.SCREEN_PREV_HEIGHT))
                print("Scale proportion: " + str(aux_rect.y / s.SCREEN_PREV_HEIGHT))
                print("New ship position: " + str(self.rect))
        
        # Update speed
        self.speed = (s.SCREEN_HEIGHT / s.SHIP_MIN_SPEED)
        
        
        # Game area
        # https://stackoverflow.com/questions/35304498/what-are-the-pygame-surface-get-rect-key-arguments
        screen = pygame.display.get_surface()
        game_area_height = s.SCREEN_HEIGHT - s.SCREEN_HUD_HEIGHT
        self.area = screen.get_rect(top = s.SCREEN_HUD_HEIGHT, 
            height = game_area_height)
            
        # IA
        # Divide into scan zones
        number_ship_height_divisions = (((s.SCREEN_HEIGHT - s.SCREEN_HUD_HEIGHT) // self.rect.height) + 1)
        
        self.scan_height = (game_area_height // number_ship_height_divisions)
        sight = self.rect.width
        max_sight = (s.SCREEN_WIDTH / 2) if (s.IA_TYPE == 2) else (s.SCREEN_WIDTH / 4)
        sight += max_sight
        
        aux_scan_rect_y = s.SCREEN_HUD_HEIGHT
        if (self.scan_area != None):
            # Update position respecting scale
            aux_scan_rect_y = 0 if (self.scan_area.y == 0) else ((self.scan_area.y / s.SCREEN_PREV_HEIGHT) * s.SCREEN_HEIGHT)
            
            # Update best gap position
            if self.found_best_gap:
                self.dest_pos_y = aux_scan_rect_y
        
        self.scan_area = pygame.Rect(self.rect.x, aux_scan_rect_y, sight, self.rect.height)
        
    
    # https://stackoverflow.com/questions/22361249/how-can-i-just-draw-a-specific-one-sprite-in-a-group-with-pygame-draw
    def draw(self, screen):
        """ Draws ship at the screen"""
        
        if not self.explosion:
        
            if self.block and not self.waiting_for_landing and not self.landing_to_planet:
                # Flickering image
                amount_frames = 5
                if (self.animation_count % amount_frames != 0):
                    screen.blit(self.image, self.rect)
            
            else:
                screen.blit(self.image, self.rect)
        
        if s.DEBUG or s.SHIP_DEBUG:
            if self.waiting_for_landing:
                pygame.draw.rect(screen, s.GREEN, self.scan_area, 1)
    
        
    def update(self):
        """ Updates ship state"""
    
        # Movement
        newpos = self.rect.move((self.movepos[0], round(self.movepos[1])))
        if (self.area.contains(newpos) and not self.block and not
            self.waiting_for_landing and not self.landing_to_planet):
            self.rect = newpos
        
        
        # Restore animation
        if self.block and not self.waiting_for_landing and not self.landing_to_planet:
        
            self.finish_animation = (self.animation_count > s.FPS * self.duration_factor)
            
            if not self.finish_animation:
                self.animation_count += 1
            else:
                self.animation_count = 0
                self.block = False
                
        # IA
        if self.waiting_for_landing or self.landing_to_planet:
            # Update ship position
            self.automove()
                  
            
    def moveup(self):
        """ Moves ship in upward direction"""
        
        if self.state[0] == "Not pressed":
            self.state[1] = "Pressed"
            self.movepos[1] = self.movepos[1] - self.speed
        
        else:
            self.state[1] = "Pressed"
            self.movepos[1] = -self.movepos[1]
    
    
    def movedown(self):
        """ Moves ship in downward direction"""
        
        if self.state[1] == "Not pressed":
            self.state[0] = "Pressed"
            self.movepos[1] = self.movepos[1] + self.speed
        
        else:
            self.state[0] = "Pressed"
            self.movepos[1] = -self.movepos[1]
    
    
    def change_speed(self, speed):
        """ Changes ship speed"""    
    
        # Reset move position
        self.movepos = [0,0]
        
        # Update speed
        self.speed = speed
        
        self.is_accelerated = (self.speed == (s.SCREEN_HEIGHT / s.SHIP_MAX_SPEED))
            
        
        # Update move position
        if self.state[1] == "Pressed":
            self.moveup()
        elif self.state[0] == "Pressed":
            self.movedown()
    
    
    def stop(self, state):
        """ Stopes the ship while up or down key are not pressed""" 

        # Reset move position
        self.movepos = [0,0]
        
        # Update move position
        if state == "Move up release":
            self.state[1] = "Not pressed"
            if self.state[0] == "Pressed":
                self.movepos[1] = self.speed 
                
        elif state == "Move down release":
            self.state[0] = "Not pressed"
            if self.state[1] == "Pressed":
                self.movepos[1] = -self.speed
    
    
    def loose_one_life(self):
        """ Activates lose one life effects"""     
    
        self.explosion = True
        self.block = True
        pygame.mixer.Sound.play(self.explosion_sound)
           
           
    def restore(self):
        """ Restores the ship after explosion"""

        self.reinit()
        
        self.explosion = False
    
    
    def start_landing(self):
        """ Prepares the ship to landing"""
    
        # Rotate ship to land
        degrees_rot = 180
        self.image, self.rect = u.rot_center(self.image, degrees_rot, self.rect.centerx, self.rect.centery)
        
        self.waiting_for_landing = True
        self.block = False
    
    
    def continue_game(self):
        """ Prepares ship to play new level"""
    
        if s.DEBUG or s.SHIP_DEBUG:
            print("Resets ship state")
        
        # Rotate ship to normal position
        if (self.waiting_for_landing or self.landing_to_planet):
            degrees_rot = 180
            self.image, self.rect = u.rot_center(self.image, degrees_rot, self.rect.centerx, self.rect.centery)
        
        self.landing_to_planet = False
        
        if (s.IA_TYPE == 2):    
            self.waiting_for_landing = False
        
        self.reinit()
    
    
    def detect_collision(self, group_sprites):
        """ Detects collision between ship and a groups of sprites"""    
    
        # Detect collision
        is_collisioned = pygame.sprite.spritecollideany(self, group_sprites) != None
        player_condition = not self.explosion and not self.block
        
        return (is_collisioned and player_condition)
    
    
    def detect_rect_collision(self, rects, group_sprites):
        """ Detects collision between ship scan area and rectangles"""
        
        # Detect collisions
        index = self.scan_area.collidelist(rects)

        return ((index == -1) or ((index != -1) and not group_sprites.sprites()[index].show))
        
    
    def continue_landing(self):
        """ Puts the ship in the state continue landing"""
    
        self.landing_to_planet = True
        self.waiting_for_landing = False

    
    def automove(self):
        """ Controls the IA's movement"""
    
        # Movement in x direction
        if not self.stopped:
            if self.landing_to_planet or (s.IA_TYPE == 2):
                if (s.IA_TYPE == 1) or ((s.IA_TYPE == 2) and self.landing_to_planet):
                    self.rect.x += (s.SCREEN_HEIGHT / s.SHIP_MAX_SPEED)
                elif (s.IA_TYPE == 2):
                    self.rect.x += 1
        
        
        # Movement in y direction
        if self.waiting_for_landing:

            # Update position
            if self.found_best_gap:
                
                stop = (self.rect.y == self.dest_pos_y)
                
                if not stop:
                    
                    speed = 1 if (abs(self.rect.y - self.scan_area.y) < (self.scan_area.height / 2)) else (s.SCREEN_HEIGHT / s.SHIP_MAX_SPEED)
                    if (self.rect.y < self.dest_pos_y):
                        self.rect.y += speed
                    elif (self.rect.y > self.dest_pos_y):
                        self.rect.y -= speed
    
    
    def scan(self):
        """ Updates scan area position"""
    
        if self.waiting_for_landing:
            
            # Update scan area
            self.scan_area.x = self.rect.x
            
            if self.finding_best_gap:
                
                self.complete_scan = ((self.scan_area.y + self.rect.height) >= (s.SCREEN_HEIGHT - s.SCREEN_HUD_HEIGHT))
                if not self.complete_scan:
                    self.scan_area.y += self.scan_height
                
                if s.DEBUG or s.SHIP_DEBUG:
                    print("Scan y position: " + str(self.scan_area.y))
                    if self.complete_scan:
                        print("Complete scan: " + str(self.complete_scan))
                       
    
    def find_best_gap(self, rects, group_sprites):
        """ Find the best gap between all the scans positions if exists. 
            The best gap is the one collision-free and closest to the ship."""
        
        # Detect collisions
        find_gap = self.detect_rect_collision(rects, group_sprites)
    
        # Are the best gap valid?
        if self.found_best_gap:

            # Look for new best gap?
            self.finding_best_gap = not find_gap
            
            # Search again for gaps
            if self.finding_best_gap:
            
                # Reset
                self.found_best_gap = False
                
                # Reset scan position
                self.scan_area.y = s.SCREEN_HUD_HEIGHT
                
                if s.DEBUG or s.SHIP_DEBUG:
                    print("Reset scan")
        
        
        # If find a new gap add to list
        if self.finding_best_gap:
        
            if s.DEBUG or s.SHIP_DEBUG:
                print("Finding gap in: " + str(self.scan_area.y))
            
            if find_gap:
                self.gaps.append(self.scan_area.y)
                
                if s.DEBUG or s.SHIP_DEBUG:
                    print("Gap founded in: " + str(self.scan_area.y))
        
        
        # After a complete scan get the best gap if exists
        if self.complete_scan:
        
            # Reset
            self.complete_scan = False
            
            if s.DEBUG or s.SHIP_DEBUG:
                print("Gaps list size: " + str(len(self.gaps)))
            
            # At least one gap is founded
            if (len(self.gaps) > 0):
                
                if s.DEBUG or s.SHIP_DEBUG:
                    for gap in self.gaps:
                        print("Player y position: " + str(self.rect.y))
                        print("Distance: " + str(abs(gap - self.rect.y)))
                        print("Gap value: " + str(gap))
                
                # Find gap with less distance to ship 
                
                # Sort gaps by distance to ship
                self.gaps.sort(key = lambda x: abs(x - self.rect.y))
                
                if s.DEBUG or s.SHIP_DEBUG:
                    print("Sorted gaps by less distance to ship: " + str(self.gaps))
                
                # Update best gap
                self.dest_pos_y = self.gaps[0]
                
                # Fix scan area to best gap position
                self.scan_area.y = self.dest_pos_y
                
                # Reset
                self.gaps.clear()
                
                # Update found best gap
                self.found_best_gap = True
                
                # Not find for more gaps
                self.finding_best_gap = False

        # Update scanning
        self.scan()

        