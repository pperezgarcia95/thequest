import pygame
import sys
import os
import random
import event_manager as em
import utilities as u
import settings as s
import exceptions as e


# Other game objects
import gameobjects.planet as planet_object
import gameobjects.asteroid as asteroid_object
import gameobjects.explosion as explosion_object




class Level(em.Observer):
    """Level manager
    
    Returns: Level object  
    Functions: reinit, resize, start_game, update, draw, reset_timers, generate_asteroid, change_level  
    Attributes: 
    
    """
    
    # Avoid to create more than one HUD
    __instance = None
    
    def __init__(self):
    
        if (Level.__instance == None):
            Level.__instance = self
            
        else:
            try:
                raise e.CreateInstanceError(str(type(self).__name__))
                
            except e.CreateInstanceError as error:
                
                print(error.message)
                
                # Exit
                pygame.quit()
                sys.exit(0)
        
        
        # Background
        self.background = pygame.Surface((s.SCREEN_WIDTH, s.SCREEN_HEIGHT))
        
        # Initialise current level
        self.current_level = s.INIT_LEVEL
        
        # Initialise planets
        self.total_planets = s.PLANET_MAX_TYPES
        
        self.planets_list = pygame.sprite.Group()
        
        
        planet_size = (s.SCREEN_HEIGHT - s.SCREEN_HUD_HEIGHT, s.SCREEN_HEIGHT - s.SCREEN_HUD_HEIGHT)
        init_position = (s.SCREEN_WIDTH, s.SCREEN_HUD_HEIGHT)
        for i in range(self.total_planets):
            planet = planet_object.Planet(init_position, planet_size, i + 1)
            self.planets_list.add(planet)
        
        
        # Initialise asteroids
        self.total_asteroids = s.MAXIMUM_NUMBER_ASTEROIDS_SHOW
        
        self.asteroids_list = pygame.sprite.Group()
        self.asteroids_rect_list = []
        
        for i in range(self.total_asteroids):
            asteroid = asteroid_object.Asteroid()
            self.asteroids_list.add(asteroid)
            self.asteroids_rect_list.append(asteroid.rect)
            
            # Update speed
            if (self.current_level > 1):
                for speed in s.ASTEROID_INITIAL_TYPE_SPEED:
                    speed[0] += (asteroid.speed / 100) * self.current_level
                    speed[1] += (asteroid.speed / 100) * self.current_level
        
        
        self.no_asteroids = False
        self.landing_event_fire = False
        self.init_asteroids_speed = s.ASTEROID_INITIAL_TYPE_SPEED
        
        
        # Initialise timer
        # https://stackoverflow.com/questions/986006/how-do-i-pass-a-variable-by-reference
        self.ref_generate_time = [pygame.time.get_ticks()]
        self.threshold_generate_time = (s.ASTEROID_INITIAL_SHOW_TIME - 
            (s.ASTEROID_INITIAL_SHOW_TIME / 10) * (self.current_level - 1))
        self.ref_change_level_time = [pygame.time.get_ticks()]
        self.threshold_change_level_time = (s.LEVEL_CHANGE_TIME +
            (s.LEVEL_CHANGE_TIME / 10) * (self.current_level - 1))
            
        self.init_threshold_generate_time = self.threshold_generate_time
        self.init_threshold_change_level_time = self.threshold_change_level_time
        
        
        self.stop_timer = False
        
        
        # Initialise animations
        self.explosion = explosion_object.Explosion()
        
        
        # Create observer
        em.Observer.__init__(self)
        
        # Subscribe to an event
        self.observe('start game', self.start_game)
        self.observe('continue game', self.change_level)
        self.observe('reinit game', self.reinit)
        
        
        if s.DEBUG or s.LEVEL_DEBUG:
            print("Level is listening.")
        
        # Create events
        self.explosion_event = em.Event('loose one life', None, False)
        self.start_landing_event = em.Event('start landing', None, False)
        self.landing_to_planet_event = em.Event('landing to planet', None, False)
        self.finish_landing_event = em.Event('finish landing', None, False)
        
        self.show_message_event = em.Event('show message', None, False)  
    
    
    def reinit(self):
        """ Resets level to init state"""
        
        # Reset level
        self.current_level = s.INIT_LEVEL
        
        # Reset asteroids speed
        s.ASTEROID_INITIAL_TYPE_SPEED = self.init_asteroids_speed
        
        for asteroid in self.asteroids_list:
            asteroid.generate_position()
        
        # Reset thresholds
        self.threshold_generate_time = self.init_threshold_generate_time
        self.threshold_change_level_time = self.init_threshold_change_level_time
        
        # Detect planet collision again
        self.landing_event_fire = False
    
    
    def resize(self):
        """ Adaptates scales and positions of the level"""
    
        # Background
        self.background = u.load_png('level_bg.png', (s.SCREEN_WIDTH, s.SCREEN_HEIGHT))[0]
        
        # Resize planets
        planet_size = (s.SCREEN_HEIGHT - s.SCREEN_HUD_HEIGHT, s.SCREEN_HEIGHT - s.SCREEN_HUD_HEIGHT)
        init_position = (s.SCREEN_WIDTH, s.SCREEN_HUD_HEIGHT)
        for planet in self.planets_list:
            planet.resize(init_position, planet_size)
            
        # Resize asteroids and update rect astetoids list
        self.asteroids_rect_list.clear()
        for asteroid in self.asteroids_list:
            asteroid.resize()
            self.asteroids_rect_list.append(asteroid.rect)        
    
    
    def start_game(self):
        """ Prepares level to first start"""
        
        # Update background
        self.background = u.load_png('level_bg.png', (s.SCREEN_WIDTH, s.SCREEN_HEIGHT))[0]
        
        self.reset_timers()
    
    
    def update(self, player):
        """ Updates level state"""
        
        # Asteroids
        self.generate_asteroid()
        self.asteroids_list.update()
        
        # Filter by showed
        filtered_asteroid_list = list(filter(lambda x: x.show, self.asteroids_list))
        
        
        if (player.detect_collision(filtered_asteroid_list)):
            if (player.waiting_for_landing or player.landing_to_planet):
               self.show_message_event.fire()
            else:
                self.explosion_event.fire()
        
        
        # Update animations
        self.explosion.update()
        self.planets_list.update()
        
        # Landing
        if player.waiting_for_landing:
            
            # Wait until not exists asteroids
            # https://dev.to/suvhotta/python-lambda-and-list-comprehension-5128
            self.no_asteroids = (len(list(filter(lambda x: x.show, self.asteroids_list))) == 0)
            
            
            if self.no_asteroids:
                if s.DEBUG or s.LEVEL_DEBUG:
                    print("Landing to planet")
                
                self.landing_to_planet_event.fire()
            
            
            # IA
            player.find_best_gap(self.asteroids_rect_list, self.asteroids_list)
            
            
        if (player.landing_to_planet or (s.IA_TYPE == 2)) and not self.landing_event_fire:
            # Detect planet collision
            if (player.detect_collision(self.planets_list)):
                
                if (player.rect.x >= (s.SCREEN_WIDTH - (player.rect.width))):
                    # Stop ship
                    player.stopped = True
                    
                    # Ensure that not exists asteroids in the screen
                    if (self.no_asteroids):
                        self.finish_landing_event.fire()
                        
                        self.landing_event_fire = True
                        
                        if s.DEBUG or s.LEVEL_DEBUG:
                            print("Fire finish landing event")
        
        
        # Start landing
        if not self.stop_timer:
            if (u.timer(self.ref_change_level_time, self.threshold_change_level_time)):
                # Launch start landing event
                self.start_landing_event.fire()
                
                # Not created more asteroids and fix the level
                self.stop_timer = True
                
                # Show random planet
                random_planet = random.randint(0, self.total_planets - 1)
                self.planets_list.sprites()[random_planet].start_animation()
        
        
    def draw(self, screen, player_position):
        """ Draws level objects at the screen"""
        
        # Background
        screen.blit(self.background, (0, 0))
            
        # Draw asteroids
        for asteroid in self.asteroids_list:
            if asteroid.show:
                asteroid.draw(screen)
                
        # Draw animations
        self.explosion.draw(screen, player_position)
        
        for planet in self.planets_list:
            if planet.show:
                planet.draw(screen)
    
    
    def reset_timers(self):
        """ Resets the timers"""
        
        self.stop_timer = False
        self.ref_generate_time[0] = pygame.time.get_ticks()
        self.ref_change_level_time[0] = pygame.time.get_ticks()
    
    
    def generate_asteroid(self):
        """ Shows a random asteroid"""
        
        # Updates asteorid state
        if not self.stop_timer:
            if (u.timer(self.ref_generate_time, self.threshold_generate_time)):
                
                random_pos = random.randint(0, self.total_asteroids - 1)
                while (self.asteroids_list.sprites()[random_pos].show):
                    random_pos = random.randint(0, self.total_asteroids - 1)

                self.asteroids_list.sprites()[random_pos].show = True
        
    
    def change_level(self):
        """ Updates current level to the next"""
        
        # Updates level state
        self.current_level += 1
        self.threshold_generate_time -= (self.threshold_generate_time / 100) * self.current_level
        self.threshold_change_level_time += (self.threshold_change_level_time / 100) * self.current_level

        
        for speed in s.ASTEROID_INITIAL_TYPE_SPEED:
            speed[0] += (speed[0] / 100) * self.current_level
            speed[1] += (speed[1] / 100) * self.current_level
        
        
        self.landing_event_fire = False
        
        
        # Reset
        self.reset_timers()
        
        
        if s.DEBUG or s.LEVEL_DEBUG:      
            print("Current level: " + str(self.current_level) + " Generate threshold: " + 
                str(self.threshold_generate_time) + " Change level threshold: " +
                str(self.threshold_change_level_time))
    
