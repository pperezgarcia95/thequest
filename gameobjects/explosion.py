import pygame
import os
import random
import event_manager as em
import utilities as u
import settings as s




# https://www.simplifiedpython.net/pygame-sprite-animation-tutorial/
class Explosion(pygame.sprite.Sprite, em.Observer):
    """ Player ship explosion animation
    
    Returns: explosion object  
    Functions: draw, update, loose_one_life  
    Attributes: 
    
    """
    
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.sheet = u.load_png('sheet_explosion.png', (0, 0), False)[0]
        
        self.images = []
        self.index = 0
        
        num_col = 10
        num_row = 5
        offset = (25, 25)
        size = (50, 50)
        self.images = u.get_sprites_sheet(self.sheet, num_col, num_row, offset, size)
        
        
        self.show = False
        self.animation_count = 0
        self.finish_animation = False

        # Create observer
        em.Observer.__init__(self)
        
        # Subscribe to an event
        self.observe('loose one life', self.loose_one_life)
        
        if s.DEBUG or s.LEVEL_DEBUG:
            print("Explosion is listening.")
        
        # Generate event
        self.restore_event = em.Event('restore', None, False)
        
        
    # https://stackoverflow.com/questions/22361249/how-can-i-just-draw-a-specific-one-sprite-in-a-group-with-pygame-draw
    def draw(self, screen, player_position):
        """ Draws the explosion at the screen"""
        
        if self.show:
            screen.blit(self.image, player_position)


    def update(self):
        """ Updates sprites of the explosion"""
    
        if self.show:
        
            # Update sprite
            self.index += 1
     
            if self.index >= len(self.images):
                self.index = 0

            self.image = self.images[self.index]
            
            
            # Check animation status
            self.finish_animation = (self.animation_count > s.FPS)
            
            if not self.finish_animation:
                self.animation_count += 1
            else:
                self.show = False
                self.animation_count = 0
                
                self.restore_event.fire()
                
                
    def loose_one_life(self):
        """ Activates the explosion at the screen"""
        
        self.show = True

