import pygame
import os
import random
import event_manager as em
import utilities as u
import settings as s




class Planet(pygame.sprite.Sprite, em.Observer):
    """Planet surface
    
    Returns: Planet object  
    Functions: reinit, resize, update, draw, start_animation  
    Attributes: fix_position, dimensions, type
    
    """
    
    def __init__(self, fix_position, dimensions, type):
        pygame.sprite.Sprite.__init__(self)
        self.rect = None
        self.name = 'planet' + str(type) + '.png'
        
        
        self.show = False
        self.move = False
        
        self.resize(fix_position, dimensions)
        
        
        # Animation
        self.animation_count = 0
        self.duration_factor = 1.5
        self.finish_animation = False
        
        
        # Create observer
        em.Observer.__init__(self)
        
        # Subcribe to different events
        self.observe('continue game', self.reinit)
        self.observe('reinit game', self.reinit)
        
        if s.DEBUG or s.LEVEL_DEBUG:
            print("Planet is listening.")
    
    
    def reinit(self):
        """ Returns planet to init state"""
        
        self.show = False
        
        # Reset position
        self.rect.x = self.init_position[0]
        self.rect.y = self.init_position[1]
    
    
    def resize(self, position, dimensions):
        """ Adaptates the planet scale, its position and its stop position for the animation"""
        
        # Load sprite
        aux_rect = None
        if (self.rect != None):
            aux_rect = self.rect
        
        self.image, self.rect = u.load_png(self.name, dimensions)
        
        # Update position
        self.init_position = position
        
        # Update position respecting scale
        if self.show:
            if (aux_rect != None):
                self.rect.x = 0 if (aux_rect.x == 0) else ((aux_rect.x / s.SCREEN_PREV_WIDTH) * s.SCREEN_WIDTH)
                self.rect.y = 0 if (aux_rect.y == 0) else ((aux_rect.y / s.SCREEN_PREV_HEIGHT) * s.SCREEN_HEIGHT)
        else:
            self.rect.x = position[0]
            self.rect.y = position[1]
        
        # Update stop position (animation)
        self.stop_position = (s.SCREEN_WIDTH - (self.rect.width / 3))
    
    
    def update(self):
        """ If it is being animated, updates current planet position"""
        
        if self.move:
            self.finish_animation = (self.animation_count > s.FPS * self.duration_factor)
            
            if not self.finish_animation:
                self.animation_count += 1
                
                # Update position
                if self.move:
                    self.rect.x -= 1
                    
                    # Check position
                    if self.rect.x < self.stop_position:
                        self.move = False
                
            else:
                # Reset
                self.animation_count = 0
    
    
    # https://stackoverflow.com/questions/22361249/how-can-i-just-draw-a-specific-one-sprite-in-a-group-with-pygame-draw
    def draw(self, screen):
        """ Draws planet at the screen"""
        
        if self.show:
            screen.blit(self.image, self.rect) 


    def start_animation(self):
        """ Triggers the planet animation"""
    
        if s.DEBUG or s.LEVEL_DEBUG:
            print("Show planet")
        
        # Start animation
        self.show = True
        self.move = True

