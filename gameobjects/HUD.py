import pygame
import sys
import os
import random
import event_manager as em
import utilities as u
import settings as s
import exceptions as e




class HUD(em.Observer):     
    """ Head-Up Display
    
    Returns: HUD object  
    Functions: reinit, resize, draw, update, increase_score, loose_one_life, show_message, hide_message  
    Attributes: level
    
    """
    
    # Avoid to create more than one HUD
    __instance = None
    
    def __init__(self, level):
        
        if (HUD.__instance == None):
            HUD.__instance = self
            
        else:
            try:
                raise e.CreateInstanceError(str(type(self).__name__))
                
            except e.CreateInstanceError as error:
                
                print(error.message)
                
                # Exit
                pygame.quit()
                sys.exit(0)
        
        
        # Create observer
        em.Observer.__init__(self)
        
        # Subcribe to different events
        self.observe('increase score', self.increase_score)
        self.observe('loose one life', self.loose_one_life)
        self.observe('show message', self.show_message)
        self.observe('finish landing', self.hide_message)
        self.observe('reinit game', self.reinit)
        
        if s.DEBUG or s.LEVEL_DEBUG:
            print("HUD is listening.")
        
        
        # Score
        self.score_value = 0
        
        # Life
        self.total_lifes = 3
        
        # Initialise
        self.resize(level)
    
    
    def reinit(self):
        """ Resets HUD to init state"""
        
        # Score
        self.score_value = 0
        
        # Life
        self.total_lifes = 3
        
        degrees_rot = 90
        life_size = s.SHIP_SIZE
        
        
        self.lifes_list = []
        for i in range(self.total_lifes):
            life_image, life_rect = u.load_png('ship.png', life_size)
            life_image, life_rect = u.rot_center(life_image, degrees_rot, life_rect.centerx, life_rect.centery)
            self.lifes_list.append((life_image, life_rect))
        
    
    def resize(self, level):
        """ Adaptates scale and positions inside the HUD"""
        
        # Score
        font_size = (s.SCREEN_HEIGHT // 22)
        self.font = pygame.font.SysFont("serif", font_size)
        self.text_score = self.font.render("Score: " + str(self.score_value), True, s.YELLOW)
        self.score_position = ((s.SCREEN_WIDTH // 40), (s.SCREEN_HEIGHT // 50))
        
        # Life
        self.text_life = self.font.render("Life: ", True, s.RED)
        
        padding_x = s.SHIP_SIZE[0] * self.total_lifes
        half_screen_width_size = (s.SCREEN_WIDTH // 2)
        half_text_life_width_size = ((self.text_life.get_width() + padding_x) // 2)
        self.lifes_start_position = half_screen_width_size + half_text_life_width_size - padding_x
        life_top_padding = (s.SCREEN_HEIGHT // 50)
        self.x_pos_life = half_screen_width_size - half_text_life_width_size
        self.y_pos_life = life_top_padding
        
        self.lifes_list = []
        
        degrees_rot = 90
        life_size = s.SHIP_SIZE
        
        # Clamp
        if life_size[1] > s.SCREEN_HUD_HEIGHT:
            padding = s.SCREEN_HUD_HEIGHT // 10
            life_size = (s.SCREEN_HUD_HEIGHT - padding, s.SCREEN_HUD_HEIGHT - padding)
        
        
        for i in range(self.total_lifes):
            life_image, life_rect = u.load_png('ship.png', life_size)
            life_image, life_rect = u.rot_center(life_image, degrees_rot, life_rect.centerx, life_rect.centery)
            self.lifes_list.append((life_image, life_rect))
        
        # Level
        level_right_padding = (s.SCREEN_WIDTH // 40)
        self.text_level = self.font.render("Level: " + str(level), True, s.BLUE)
        self.x_pos_level = s.SCREEN_WIDTH - self.text_level.get_width() - level_right_padding
        
        # IA message
        self.show_text_message = False
        
        self.text_message = self.font.render("Ok, Houston, we've had a problem here.", True, s.YELLOW)
        
        half_screen_height_size = (s.SCREEN_HEIGHT // 2)
        half_text_message_width_size = (self.text_message.get_width() // 2)
        hallf_text_message_height_size = (self.text_message.get_height() // 2)
        self.x_pos_message = half_screen_width_size - half_text_message_width_size
        self.y_pos_message = half_screen_height_size - hallf_text_message_height_size
        
    
    # https://stackoverflow.com/questions/22361249/how-can-i-just-draw-a-specific-one-sprite-in-a-group-with-pygame-draw
    def draw(self, screen):
        """ Draws the HUD on the screen"""
        
        # Score
        screen.blit(self.text_score, self.score_position)
        
        # Life
        screen.blit(self.text_life, (self.x_pos_life, self.y_pos_life))
        
        for life in self.lifes_list:
            screen.blit(life[0], (life[1].x, life[1].y))
        
        
        # Level
        level_pos_y = (s.SCREEN_HEIGHT // 50)
        screen.blit(self.text_level, (self.x_pos_level, level_pos_y))
        
        
        # Message
        if self.show_text_message:
            screen.blit(self.text_message, (self.x_pos_message, self.y_pos_message))
    
    
    def update(self, level):
        """ Updates the state of the HUD"""

        # Score
        self.text_score = self.font.render("Score: " + str(self.score_value), True, s.YELLOW)
        
        # Life
        displace = 0
        for life in self.lifes_list:
            
            life[1].x = self.lifes_start_position + displace
            
            displace += life[1].size[0]
        
        
        # Level
        self.text_level = self.font.render("Level: " + str(level), True, s.BLUE)
        
        
    def increase_score(self, amount):
        """ Increases the HUD score in an indicate amount"""
        
        self.score_value += amount
    
    
    def loose_one_life(self):
        """ Decreases one life in the HUD"""
        
        last_element = -1
        
        for i in range(s.LIFE_DECREASE):
            if len(self.lifes_list) > 0:
                del self.lifes_list[last_element]
        
        self.total_lifes -= s.LIFE_DECREASE
        
    
    def show_message(self):
        """ Shows a message related with the IA"""
        
        self.show_text_message = True
        
    
    def hide_message(self):
        """ Hides a message related with the IA"""
        
        self.show_text_message = False

