# https://docs.python.org/3/tutorial/errors.html
# https://www.geeksforgeeks.org/user-defined-exceptions-python-examples/


class CreateInstanceError(Exception):
    """ Exception raised when create more than one instance for the same object. 
       Only one instance are permitted to create for one time.
       
       Attributes: name
       
    """
    
    def __init__(self, name):
        self.message = ("Error when create an instance for " + name + 
            ". Previous instance was created. Only one instance are permitted to create for one time.")
        
        