"""
 Author: Paula Perez Garcia  
 Date created: 15/06/2021  
 Date last modified: 23/07/2021  
 Python Version: 3.9.6

 Released under the GNU General Public License
 
"""


# Code templates utilized
# https://www.pygame.org/docs/tut/tom_games6.html#makegames-6-3
# https://github.com/mundo-python/pygame-Scripts/blob/master/17_game_over.py

# Guides
# https://www.python.org/dev/peps/pep-0008/#class-names
# https://github.com/naming-convention/naming-convention-guides/tree/master/python
# https://gameprogrammingpatterns.com/contents.html
# https://sqlite.org/lang.html
# https://www.pygame.org/docs

# Resources
# https://opengameart.org
# https://sqlitebrowser.org/



VERSION = "0.1"

try:
    import sys
    import random
    import re
    import pygame
    
    import gameobjects.level as level_object
    import gameobjects.HUD as HUD_object
    import gameobjects.UI as UI_object
    import gameobjects.ship as ship_object
    
    import db.database as db
    
    import settings as s
    import event_manager as em
    import utilities as u
    import exceptions as e

    from pygame.locals import *
except ImportError as err:
    print ("couldn't load module. %s" % err)
    sys.exit(2)


    

class Game(em.Observer):
    """Game manager
    
    Returns: Game object  
    Functions: resize_screen, reset_timer, process_inputs, update_state, render_frame  
    Attributes:
    
    """
    
    # Avoid to create more than one HUD
    __instance = None
    
    def __init__(self):
    
        if (Game.__instance == None):
            Game.__instance = self
            
        else:
            try:
                raise e.CreateInstanceError(str(type(self).__name__))
                
            except e.CreateInstanceError as error:
                
                print(error.message)
                
                # Exit
                pygame.quit()
                sys.exit(0)
        
        
        self.game_over = False
        
        # Initialise screen
        # https://stackoverflow.com/questions/11603222/allowing-resizing-window-pygame 
        self.screen = pygame.display.set_mode((s.SCREEN_WIDTH, s.SCREEN_HEIGHT), pygame.RESIZABLE)
        pygame.display.set_caption('The quest')
        
        self.is_resize_screen = False
        
        
        # Initialise player
        self.player = ship_object.Ship(s.SHIP_SIZE)
        
        # Initialise Level
        self.level = level_object.Level()

        # Initialise HUD
        self.HUD = HUD_object.HUD(self.level.current_level)
        
        # Initialise UI
        self.UI = UI_object.UI()
        
        # Timer
        self.ref_change_to_scoreboard_time = [pygame.time.get_ticks()]
        self.threshold_change_to_scoreboard_time = s.CHANGE_TO_SCOREBOARD_TIME
        
        
        # Create observer
        em.Observer.__init__(self)
        
        # Subcribe to different events
        self.observe('reinit game', self.reset_timer)
        
        if s.DEBUG:
            print("Game is listening.")
        
        
        # Create events
        self.continue_game_event = em.Event('continue game', None, False)
        self.game_over_event = em.Event('game over', None, False)
        
        
        # Initaliase database
        self.connection = db.sql_connection()

        db.sql_table(self.connection)      
    
    
    def resize_screen(self, width, height):
        """ Updates screen's width and height with the current window values"""
    
        self.screen = pygame.display.set_mode((width, height), pygame.RESIZABLE)
        s.SCREEN_PREV_WIDTH = s.SCREEN_WIDTH
        s.SCREEN_WIDTH = width
        s.SCREEN_PREV_HEIGHT = s.SCREEN_HEIGHT
        s.SCREEN_HEIGHT = height
        s.SCREEN_HUD_HEIGHT = (s.SCREEN_HEIGHT // 10)
        
        if s.DEBUG or s.LEVEL_DEBUG:
            print("HUD height: " + s.SCREEN_HUD_HEIGHT)
        
        self.is_resize_screen = True
    
    
    def reset_timer(self):
        """ Resets the timer that cause to display the scoreboard"""
        
        self.ref_change_to_scoreboard_time = [pygame.time.get_ticks()]
    
    
    def process_inputs(self):
        """ Manages user inputs"""
    
        for event in pygame.event.get():
            if (event.type == QUIT):
                return True
            
            # Resize window
            if (event.type == pygame.VIDEORESIZE):
                self.resize_screen(event.w, event.h)
            
            if (event.type == KEYDOWN):
                # Edit scoreboard
                # https://pygame.readthedocs.io/en/latest/4_text/text.html
                if (self.UI.state == "Show scoreboard"):
                    if (event.key == K_BACKSPACE):
                        if len(self.UI.text_name_current_score) > 0:
                            self.UI.text_name_current_score = self.UI.text_name_current_score[:-1]
                    
                    elif (event.key == s.START_KEY):

                        # https://stackoverflow.com/questions/12595051/check-if-string-matches-pattern
                        # https://docs.python.org/3/library/re.html
                        if ((self.UI.found_scoreboard_pos) and 
                            (re.search("([A-Z]|[a-z]){3}", self.UI.text_name_current_score) != None)):
                        
                            # Update database
                            values = [(self.UI.text_name_current_score.upper()), (self.UI.score)]
                            u.update_database(self.connection, values, len(self.UI.rows))
                            
                            
                            self.UI.update_database = True
                            
                            if s.DEBUG or s.DATABASE_DEBUG:
                                print("Update database")
                            
                            
                            # Show scoreboard
                            values = [(0), (self.connection)]
                            self.UI.show_scoreboard(values)
                            
                            
                    elif (len(self.UI.text_name_current_score) < s.MAX_NUM_CHAR):
                        self.UI.text_name_current_score += event.unicode
                        
                    # Update name text current score
                    self.UI.update_name_current_score()
                    
                
                # Move player
                if ((event.key == K_UP) or (event.key == K_w)):
                    self.player.moveup()
                elif ((event.key == K_DOWN) or (event.key == K_s)):
                    self.player.movedown()
                
                # Accelerate
                if ((event.key == K_LSHIFT) or (event.key == K_RSHIFT)):
                    
                    # Change speed
                    if not self.player.is_accelerated:
                        self.player.change_speed(s.SCREEN_HEIGHT / s.SHIP_MAX_SPEED)
                    
                # Quit game                
                if (event.key == pygame.K_ESCAPE):
                    return True

                # Start game
                if (event.key == s.START_KEY):
                    if (self.UI.state == "Init"):
                        if s.DEBUG or s.UI_DEBUG:
                            print("Show instructions")
                        
                        self.UI.show_instructions()
                            
                
                # Next level
                if (event.key == s.CONTINUE_KEY):
                    if (self.UI.state == "Continue message"):
                        self.continue_game_event.fire()
                        
                        if s.DEBUG or s.LEVEL_DEBUG:
                            print("Continue game event fire")
            
            
            if (event.type == KEYUP):
                if ((event.key == K_UP) or (event.key == K_w)):
                    # Stop going up
                    self.player.stop("Move up release")
                elif ((event.key == K_DOWN) or (event.key == K_s)):
                    # Stop going down
                    self.player.stop("Move down release")
                
                if ((not (event.mod & pygame.KMOD_SHIFT)) and
                    ((event.key == K_LSHIFT) or (event.key == K_RSHIFT))):
                    
                    # Reset speed
                    self.player.change_speed(s.SCREEN_HEIGHT / s.SHIP_MIN_SPEED)
                    
    
    def update_state(self):
        """ Updates the game state"""
        
        # Game over
        self.game_over = (self.HUD.total_lifes <= 0) and not self.player.explosion
                
        if not self.game_over:
            
            if (self.UI.state == "Hide"):
                
                # Player
                self.player.update()
                
                # Level
                self.level.update(self.player)
                
                # Update HUD
                self.HUD.update(self.level.current_level)
                
            elif (self.UI.state == "Init"):
                
                if (u.timer(self.ref_change_to_scoreboard_time, self.threshold_change_to_scoreboard_time)):
                    # Show scoreboard
                    values = [(0), (self.connection)]
                    self.UI.show_scoreboard(values)
            
            
            # Update UI
            self.UI.update()
            
            # Resize
            if self.is_resize_screen:
                
                # Ship
                s.SHIP_SIZE = ((s.SCREEN_WIDTH // 15), (s.SCREEN_WIDTH // 15))
                
                if s.DEBUG or s.SHIP_DEBUG:
                    print("Ship size: " + str(s.SHIP_SIZE))
                    
                self.player.resize(s.SHIP_SIZE)
                
                # UI
                self.UI.resize()
                
                # HUD
                self.HUD.resize(self.level.current_level)
                
                # Level
                self.level.resize()
                
                # Reset
                self.is_resize_screen = False
        
        else:
            if (self.UI.state != "Show scoreboard"):
                # Update data event
                self.game_over_event.data = [self.HUD.score_value, self.connection]
                
                # Fire event
                self.game_over_event.fire()
            
            else:
                # Update UI
                self.UI.update()
                
                # Resize
                if self.is_resize_screen:
                    self.UI.resize()
                    
                # Reset
                self.is_resize_screen = False
    
    
    def render_frame(self):
        """ Draws the current frame at the screen"""
        
        if not self.game_over:
            
            if ((self.UI.state == "Hide") or (self.UI.state == "Continue message")):
                
                 # Draw level
                self.level.draw(self.screen, self.player.rect)
                
                # Draw player
                self.player.draw(self.screen)
            
                # Draw HUD
                self.HUD.draw(self.screen)
            
            # Draw UI
            self.UI.draw(self.screen, self.connection)
        
        else:
            if (self.UI.state == "Show scoreboard"):
                self.UI.draw(self.screen, self.connection)
        
        pygame.display.flip()    
      

def main():
    
    pygame.init()
    
    # Initialise sound
    # https://opensource.com/article/20/9/add-sound-python-game
    pygame.mixer.init()
    
    
    # Game manager
    game = Game()
    
    # Initialise clock
    clock = pygame.time.Clock()
    
    # Game loop
    done = False
    while not done:
        # Make sure game doesn't run at more than n frames per second
        clock.tick(s.FPS)
        
        done = game.process_inputs()
        game.update_state()
        game.render_frame()

    pygame.quit()


if __name__ == '__main__': main()

