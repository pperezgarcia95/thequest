# The quest

The quest is a spacial game made it with pygame.

## Requirements

Download and install pyhton.
Then use the package manager pip to install pygame.

```bash
pip3 install pygame
```

## Usage

Locate in the main direcoty and execute:

```bash
python3 main.py
```

## IA

Changes IA_TYPE in settings.py to 1 or 2 to select between differents IA behaviours.

## Database

Needed database and tables are created automatically the first time that it's run the game, if are not exists yet.
If you experiment any kind of problems related with the automatic creation, move the database from the directory **migrations** to the main directory.

In case the problems persists, use the next sql command to create the table in the database.

```sql
CREATE TABLE IF NOT EXISTS scoreboard(id integer PRIMARY KEY, name text, score integer)
```

## Future improvements

* Ship can shoot
* Ship can move in x direction
* Add a second player

## License

[GPLv3](https://www.gnu.org/licenses/gpl-3.0.html)